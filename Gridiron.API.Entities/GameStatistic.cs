﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class GameStatistic : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string GameRefNum { get; set; }
        public int? Attendance { get; set; }
        public int? Duration { get; set; }
    }
}
