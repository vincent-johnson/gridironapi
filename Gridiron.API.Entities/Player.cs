﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class Player : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RefNum { get; set; }
        [Required]
        public string TeamRefNum { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JerseyNum { get; set; }
        public string Class { get; set; }
        public string Position { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Town { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PrevSchool { get; set; }
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
