﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class Stadium : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RefNum { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? Capacity { get; set; }
        public string Surface { get; set; }
        public string YearOpen { get; set; }
    }
}
