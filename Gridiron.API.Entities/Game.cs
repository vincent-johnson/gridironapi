﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class Game : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RefNum { get; set; }
        public DateTime Date { get; set; }
        [Required]
        public string VisitTeamRefNum { get; set; }
        [Required]
        public string HomeTeamRefNum { get; set; }
        [Required]
        public string StadiumRefNum { get; set; }
        public string Site { get; set; }
    }
}
