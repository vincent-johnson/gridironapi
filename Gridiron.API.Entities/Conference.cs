﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class Conference : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RefNum { get; set; }
        public string Name { get; set; }
        public string Subdivision { get; set; }
        public string Image { get; set; }
    }
}
