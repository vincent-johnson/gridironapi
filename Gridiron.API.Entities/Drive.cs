﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class Drive : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string GameRefNum { get; set; }
        [Required]
        public int DriveNum { get; set; }
        [Required]
        public string TeamRefNum { get; set; }
        public string StartPer { get; set; }
        public string StartClock { get; set; }
        public string StartSpot { get; set; }
        public string StartReason { get; set; }
        public string EndPer { get; set; }
        public string EndClock { get; set; }
        public string EndSpot { get; set; }
        public string EndReason { get; set; }
        public int? Plays { get; set; }
        public int? Yards { get; set; }
        public int? TimePoss { get; set; }
        public bool RedZoneAtt { get; set; }
    }
}
