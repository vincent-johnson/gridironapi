﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Entities
{
    public sealed class Team : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RefNum { get; set; }
        [Required]
        public string Name { get; set; }
        public string SchoolName { get; set; }
        [Required]
        public string ConfRefNum { get; set; }
        public int? NumOfNatlChamp { get; set; }
        public int? NumOfConfChamp { get; set; }
        public string YearFounded { get; set; }
        public int? AllTimeWins { get; set; }
        public int? AllTimeLosses { get; set; }
        public int? AllTimeTies { get; set; }
        public int? BowlWins { get; set; }
        public int? BowlLosses { get; set; }
        public int? BowlTies { get; set; }
        public int? NumOfHeismans { get; set; }
        public int? NumOfAllAmericans { get; set; }
        public string HeadCoach { get; set; }
        public string OffCoord { get; set; }
        public string DefCoord { get; set; }
        public string Mascot { get; set; }
        public string TeamUrl { get; set; }
        public string ColorHexCode { get; set; }
        public string Image { get; set; }
    }
}
