﻿using Gridiron.API.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Data
{
    public class GridironRepository<T> : 
        IGridironRepository<T> where  T : class
    {

        private readonly GridironContext _context;

        public GridironRepository()
        {
            _context = new GridironContext();
        }

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = _context.Set<T>();
            return query;
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T,bool>> predicate)
        {
            IQueryable<T> query = _context.Set<T>().Where(predicate);
            return query;
        }

        public virtual Team GetTeamByName(string name) //Most of the API resources will require a team. Allowing to lookup by name is just for ease of access
        {
            var query = (from team in _context.Teams
                         where team.Name == name.ToString()
                         select team).SingleOrDefault();
            return query;
        }
    }
}
