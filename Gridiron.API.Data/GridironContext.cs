﻿using Gridiron.API.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gridiron.API.Data
{
    public class GridironContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Conference> Conferences { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Stadium> Stadia { get; set; }
    }
}
