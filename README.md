Gridiron API
========

Gridiron API is a REST-ful API for college football statistics and information. It has several endpoints for gathering data about teams, players, conferences, games, etc. 

Full documentation is included in the project, however I will be updating this readme as the project is built out.

![Home](http://i.imgur.com/FgyXVVW.jpg)


Documentation Page for the APIs will look like the following

![Help](http://i.imgur.com/IZan4Cd.jpg)

Example JSON response for Teams API

![JsonTeam](http://i.imgur.com/lJ3OJtL.jpg)
