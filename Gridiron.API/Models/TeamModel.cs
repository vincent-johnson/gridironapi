﻿using Gridiron.API.Entities;
using Gridiron.API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gridiron.API.Models
{
    public class TeamModel
    {
        private readonly Team _team;
        private readonly IGridironRepository<Team> _repo;


        public TeamModel(int id, IGridironRepository<Team> repo)
        {
            _repo = repo;
            _team = _repo.FindBy(team => team.Id == id).SingleOrDefault();
        }

        public TeamModel(string name, IGridironRepository<Team> repo)
        {
            _repo = repo;
            _team = _repo.FindBy(team => team.Name.Replace(" ", string.Empty) == name).SingleOrDefault();
        }

        
    }
}