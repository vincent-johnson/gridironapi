﻿using Gridiron.API.Entities;
using Gridiron.API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gridiron.API.Controllers
{
    public class TeamsController : ApiController
    {
        private IGridironRepository<Team> _repo;

        public TeamsController()
        {
            _repo = new GridironRepository<Team>();
        }

        /// <summary>
        /// Returns all teams
        /// </summary>
        /// <returns>List of teams</returns>
        public IEnumerable<Team> Get()
        {
            var query = _repo.GetAll()
                        .OrderBy(team => team.Name)
                        .ToList();
            return query;
        }

        /// <summary>
        /// Returns individual Team
        /// </summary>
        /// <param name="id">Id of team</param>
        /// <returns>Individual Team Information</returns>
        public Team Get(int id)
        {
            var query = _repo.FindBy(team => team.Id == id)
                        .SingleOrDefault();

            return query;
        }

        /// <summary>
        /// Returns Individual Team
        /// </summary>
        /// <param name="name">Name of team</param>
        /// <returns>Individual Team Information</returns>
        public Team Get(string name)
        {
            var query = _repo.FindBy(team => team.Name.Replace(" ",string.Empty) == name)
                        .SingleOrDefault();

            return query;
        }

        //// POST api/teams
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/teams/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/teams/5
        //public void Delete(int id)
        //{
        //}
    }
}
