﻿using Gridiron.API.Data;
using Gridiron.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gridiron.API.Controllers
{
    public class GameDetailsController : ApiController
    {
        private IGridironRepository<GameStatistic> _repo;

        public GameDetailsController()
        {
            _repo = new GridironRepository<GameStatistic>();
        }

        /// <summary>
        /// Returns Game Details from game Id
        /// </summary>
        /// <param name="gameId">Game Identification Number</param>
        /// <returns>Individual Game Detail</returns>
        public GameStatistic Get(int gameId)
        {
            var query = _repo.FindBy(x => x.GameRefNum == gameId.ToString())
                             .SingleOrDefault();
            return query;
        }

        /// <summary>
        /// Returns Game Details from Teams 
        /// </summary>
        /// <param name="homeTeamId"></param>
        /// <param name="visitingTeamId"></param>
        /// <param name="gameDate"></param>
        /// <returns></returns>
        public GameStatistic Get(int homeTeamId, int visitingTeamId, DateTime gameDate)
        {
            var games = new GamesController();
            Game game = games.Get(homeTeamId, visitingTeamId, gameDate);
            return Get(game.Id);
        }
    }
}
