﻿using Gridiron.API.Data;
using Gridiron.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gridiron.API.Controllers
{
    public class PlayersController : ApiController
    {
        private IGridironRepository<Player> _repo;

        public PlayersController()
        {
            _repo = new GridironRepository<Player>();
        }

        /// <summary>
        /// Returns top 25 players by alphabetical order
        /// </summary>
        /// <returns>List of players</returns>
        public IEnumerable<Player> Get()
        {
            var query = _repo.GetAll()
                        .OrderBy(player => player.FullName)
                        .Take(25)
                        .ToList();
            return query;
        }

        /// <summary>
        /// Returns individual player based on id
        /// </summary>
        /// <param name="id">Id of player</param>
        /// <returns>Individual Player Information</returns>
        public Player Get(int id)
        {
            var query = _repo.FindBy(player => player.Id == id)
                        .SingleOrDefault();

            return query;
        }

        /// <summary>
        /// Returns Individual Player
        /// </summary>
        /// <param name="name">Name of Player</param>
        /// <returns>Individual Player Information</returns>
        public Player Get(string name)
        {
            var query = _repo.FindBy(player => player.FullName.Replace(" ",string.Empty) == name)
                        .SingleOrDefault();

            return query;
        }

        //// POST api/teams
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/teams/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/teams/5
        //public void Delete(int id)
        //{
        //}
    }
}
