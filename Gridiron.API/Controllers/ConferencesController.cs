﻿using Gridiron.API.Data;
using Gridiron.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gridiron.API.Controllers
{
    public class ConferencesController : ApiController
    {
        private IGridironRepository<Conference> _repo;

        public ConferencesController()
        {
            _repo = new GridironRepository<Conference>();
        }

        /// <summary>
        /// Returns all Conferences
        /// </summary>
        /// <returns>List of Conferences</returns>
        public IEnumerable<Conference> Get()
        {
            var query = _repo.GetAll()
                        .OrderBy(conference => conference.Name)
                        .ToList();
            return query;
        }

        /// <summary>
        /// Returns individual Conference
        /// </summary>
        /// <param name="id">Id of Conference</param>
        /// <returns>Individual Conference Information</returns>
        public Conference Get(int id)
        {
            var query = _repo.FindBy(conference => conference.Id == id)
                        .SingleOrDefault();

            return query;
        }

        /// <summary>
        /// Returns Individual Conference
        /// </summary>
        /// <param name="name">Name of Conference</param>
        /// <returns>Individual Conference Information</returns>
        public Conference Get(string name)
        {
            var query = _repo.FindBy(conference => conference.Name.Replace(" ",string.Empty) == name)
                        .SingleOrDefault();

            return query;
        }

        //// POST api/Conferences
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/Conferences/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/Conferences/5
        //public void Delete(int id)
        //{
        //}
    }
}
