﻿using Gridiron.API.Data;
using Gridiron.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gridiron.API.Controllers
{
    public class GamesController : ApiController
    {
        private IGridironRepository<Game> _repo;

        public GamesController()
        {
            _repo = new GridironRepository<Game>();
        }

        /// <summary>
        /// Returns all games played by team
        /// </summary>
        /// <returns>List of Games</returns>
        public IEnumerable<Game> Get(string teamName)
        {
            var query = _repo.FindBy(game => game.HomeTeamRefNum == teamName || game.VisitTeamRefNum == teamName)
                        .OrderBy(game => game.Date)
                        .Take(25)
                        .ToList();
            return query;
        }

        /// <summary>
        /// Returns all games played by team against another team
        /// </summary>
        /// <param name="teamName">Name of Home Team</param>
        /// <param name="opponentName">Name of Visiting Team</param>
        /// <returns>List of Games</returns>
        public IEnumerable<Game> Get(string teamName, string opponentName)
        {
            var HomeTeamId = _repo.GetTeamByName(teamName).Id;
            var OpponentTeamId = _repo.GetTeamByName(opponentName).Id;
            var queryOne = _repo.FindBy(game => game.HomeTeamRefNum == HomeTeamId.ToString() && game.VisitTeamRefNum == OpponentTeamId.ToString())
                        .OrderBy(game => game.Date)
                        .ToList();
            var queryTwo = _repo.FindBy(game => game.VisitTeamRefNum == teamName && game.HomeTeamRefNum == opponentName)
                        .OrderBy(game => game.Date)
                        .ToList();
            var gameList = queryOne.Union(queryTwo);//.OrderBy(game => game.Date);
            return gameList;
        }

        /// <summary>
        /// Returns all games played by team
        /// </summary>
        /// <returns>List of Games</returns>
        public IEnumerable<Game> Get(int teamId)
        {
            var query = _repo.FindBy(game => game.HomeTeamRefNum == teamId.ToString()|| game.VisitTeamRefNum == teamId.ToString())
                        .OrderBy(game => game.Date)
                        .Take(25)
                        .ToList();
            return query;
        }

        /// <summary>
        /// Returns all games played by team against another team
        /// </summary>
        /// <param name="teamName">Name of Home Team</param>
        /// <param name="opponentName">Name of Visiting Team</param>
        /// <returns>List of Games</returns>
        public IEnumerable<Game> Get(int teamId, int opponentId)
        {
            var queryOne = _repo.FindBy(game => game.HomeTeamRefNum == teamId.ToString() && game.VisitTeamRefNum == opponentId.ToString())
                        .OrderBy(game => game.Date)
                        .ToList();
            var queryTwo = _repo.FindBy(game => game.VisitTeamRefNum == teamId.ToString() &&  game.HomeTeamRefNum == opponentId.ToString())
                        .OrderBy(game => game.Date)
                        .ToList();
            var gameList = queryOne.Union(queryTwo);//.OrderBy(game => game.Date);
            return gameList;
        }

         /// <summary>
         /// Returns individual game.
         /// </summary>
         /// <param name="homeTeamId">Home Team Id</param>
         /// <param name="visitingTeamId">Visiting Team Id</param>
         /// <param name="gameDate">Date of Game (mm/dd/yyyy)</param>
         /// <returns></returns>
        public Game Get(int homeTeamId,  int visitingTeamId, DateTime gameDate)
        {
            var query = _repo.FindBy(game => game.HomeTeamRefNum == homeTeamId.ToString() && game.VisitTeamRefNum == visitingTeamId.ToString() && game.Date == gameDate)
                        .SingleOrDefault();

            return query;
        }


        //// POST api/teams
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/teams/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/teams/5
        //public void Delete(int id)
        //{
        //}
    }
}
